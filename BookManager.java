import java.util.ArrayList;

public class BookManager implements BookManagerInterface {
    private CatalogComponent catalog;

    public BookManager(CatalogComponent catalog) {
        this.catalog = catalog;
    }

    @Override
    public ArrayList<Book> searchByTitle(String title) {
        return catalog.searchByTitle(title);
    }

    @Override
    public ArrayList<Book> searchByAuthor(String name) {
        return catalog.searchByAuthor(name);
    }

    @Override
    public Book searchByIsbn(String isbn) {
        return catalog.searchByIsbn(isbn);
    }

    @Override
    public Category findCategoryByName(String name) {
        return catalog.findCategoryByName(name);
    }
}
