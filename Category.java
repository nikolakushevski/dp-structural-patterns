import javax.xml.catalog.Catalog;
import java.util.ArrayList;
import java.util.Iterator;

public class Category extends CatalogComponent {
    private String name;
    private ArrayList<CatalogComponent> catalog;

    public Category(String name) {
        this.name = name;
        this.catalog = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ArrayList<Book> searchByTitle(String title) {
        ArrayList<Book> bookList = new ArrayList<>();
        Iterator<CatalogComponent> iterator = this.catalog.iterator();
        while (iterator.hasNext()) {
            CatalogComponent component = iterator.next();
            bookList.addAll(component.searchByTitle(title));
        }
        return bookList;
    }

    @Override
    public ArrayList<Book> searchByAuthor(String name) {
        ArrayList<Book> bookList = new ArrayList<>();
        Iterator<CatalogComponent> iterator = this.catalog.iterator();
        while (iterator.hasNext()) {
            CatalogComponent component = iterator.next();
            bookList.addAll(component.searchByAuthor(name));
        }
        return bookList;
    }

    @Override
    public Book searchByIsbn(String isbn) {
        Iterator<CatalogComponent> iterator = this.catalog.iterator();
        while (iterator.hasNext()) {
            CatalogComponent component = iterator.next();
            Book book = component.searchByIsbn(isbn);
            if (book != null) {
                return book;
            }
        }
        return null;
    }

    public Category findCategoryByName(String name) {
        if (this.name.equals(name)) {
            return this;
        }

        Iterator<CatalogComponent> iterator = this.catalog.iterator();
        while (iterator.hasNext()) {
            CatalogComponent component = iterator.next();
            try {
                Category category = component.findCategoryByName(name);
                if (category != null) {
                    return category;
                }
            } catch (UnsupportedOperationException exception) {}
        }

        return null;
    }

    @Override
    public void add(CatalogComponent component) {
        this.catalog.add(component);
    }

    @Override
    public void remove(CatalogComponent component) {
        this.catalog.remove(component);
    }
}
