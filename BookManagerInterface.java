import java.util.ArrayList;

public interface BookManagerInterface {
    ArrayList<Book> searchByTitle(String title);
    ArrayList<Book> searchByAuthor(String name);
    Book searchByIsbn(String isbn);

    Category findCategoryByName(String name);
}
