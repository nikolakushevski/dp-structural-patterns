import java.time.LocalDateTime;
import java.util.ArrayList;

public class UserManager implements UserManagerInterface {
    private ArrayList<User> users;

    public UserManager() {
        this.users = new ArrayList<>();
    }

    public void add(User user) {
        users.add(user);
    }

    public void remove(User user) {
        users.remove(user);
    }

    public void rentBook(User user, Book book) {
        user.addHistory(book, LocalDateTime.now());
    }
}
