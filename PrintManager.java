public class PrintManager {
    public Card printCard(User user, Book book) {
        return new Card(user.getName(), book.getTitle());
    }
}
