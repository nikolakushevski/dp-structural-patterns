import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        CatalogComponent catalog = new Category("root");
        BookManagerInterface bookManager = new BookManager(catalog);

        UserManagerInterface userManager = new UserManager();

        PrintManagerInterface printManager = new PrintManagerAdapter(new PrintManager());

        LibraryFacade library = new LibraryFacade(bookManager, userManager, printManager);

        Category category = new Category("Anti War");
        library.addCategory(category, "root");

        ArrayList<Author> authors = new ArrayList<>();
        authors.add(new Author("Joseph Heller", 1923));
        Book book = new Book("Catch-22", 1962, "9780099477310", "Hilarious and tragic, at the heart of Catch-22 is a savage indictment of twentieth-century madness, and a desire of the ordinary man to survive it.", authors, new Publisher("Jonathan Cape Ltd", "London", "Great Britain"));

        library.addBook(book, "Anti War");

        User user = library.addUser("Nikola Kushevski");
        Book searchBook = library.searchByIsbn("9780099477310");
        System.out.println(library.rentBook(user, searchBook));
    }
}