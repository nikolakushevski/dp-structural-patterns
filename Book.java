import java.util.ArrayList;
import java.util.List;
public class Book extends CatalogComponent {
    private String title;
    private int year;
    private String isbn;
    private String description;
    private List<Author> authors;
    private Publisher publisher;

    public Book(String title, int year, String isbn, String description, List<Author> authors, Publisher publisher) {
        this.title = title;
        this.year = year;
        this.isbn = isbn;
        this.description = description;
        this.authors = authors;
        this.publisher = publisher;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public ArrayList<Book> searchByTitle(String title) {
        if (this.title.equals(title)) {
            ArrayList<Book> list = new ArrayList<>();
            list.add(this);
            return list;
        }
        return new ArrayList<>();
    }

    @Override
    public ArrayList<Book> searchByAuthor(String name) {
        for (Author author : this.authors) {
            if (author.getName().equals(name)) {
                ArrayList<Book> list = new ArrayList<>();
                list.add(this);
                return list;
            }
        }
        return new ArrayList<>();
    }

    @Override
    public Book searchByIsbn(String isbn) {
        if (this.isbn.equals(isbn)) {
            return this;
        }
        return null;
    }
}
