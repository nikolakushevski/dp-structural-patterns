import java.util.ArrayList;

public abstract class CatalogComponent {
    public ArrayList<Book> searchByTitle(String title) {
        throw new UnsupportedOperationException();
    }
    public ArrayList<Book> searchByAuthor(String name) {
        throw new UnsupportedOperationException();
    }
    public Book searchByIsbn(String isbn) {
        throw new UnsupportedOperationException();
    }
    public Category findCategoryByName(String name) {
        throw new UnsupportedOperationException();
    }
    public void add(CatalogComponent component) {
        throw new UnsupportedOperationException();
    }
    public void remove(CatalogComponent component) {
        throw new UnsupportedOperationException();
    }
}
