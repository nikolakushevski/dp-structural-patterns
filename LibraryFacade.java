import java.util.ArrayList;

public class LibraryFacade {
    private BookManagerInterface bookManager;
    private UserManagerInterface userManager;
    private PrintManagerInterface printManager;

    public LibraryFacade(BookManagerInterface bookManager, UserManagerInterface userManager, PrintManagerInterface printManager) {
        this.bookManager = bookManager;
        this.userManager = userManager;
        this.printManager = printManager;
    }

    public void addBook(Book book, String categoryName) {
        Category category = this.bookManager.findCategoryByName(categoryName);
        if (category == null) {
            throw new IllegalArgumentException();
        }

        category.add(book);
    }

    public void addCategory(Category category, String parent) {
        Category parentCategory = this.bookManager.findCategoryByName(parent);
        if (parentCategory == null) {
            throw new IllegalArgumentException();
        }

        parentCategory.add(category);
    }

    public void updateCategory(String oldName, String newName) {
        Category category = this.bookManager.findCategoryByName(oldName);
        if (category == null) {
            throw new IllegalArgumentException();
        }

        category.setName(newName);
    }

    public User addUser(String name) {
        User user = new User(name);
        this.userManager.add(user);
        return user;
    }

    public ArrayList<Book> searchByTitle(String title) {
        return bookManager.searchByTitle(title);
    }

    public ArrayList<Book> searchByAuthor(String name) {
        return bookManager.searchByAuthor(name);
    }

    public Book searchByIsbn(String isbn) {
        return bookManager.searchByIsbn(isbn);
    }

    public Card rentBook(User user, Book book) {
        userManager.rentBook(user, book);
        return printManager.print(user, book);
    }
}
