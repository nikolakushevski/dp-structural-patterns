public class Card {
    private String userName;
    private String bookTitle;

    public Card(String userName, String bookTitle) {
        this.userName = userName;
        this.bookTitle = bookTitle;
    }

    @Override
    public String toString() {
        return "Card for user: " + userName +", book: " + bookTitle;
    }
}
