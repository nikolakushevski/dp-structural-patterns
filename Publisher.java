public class Publisher {
    private String name;
    private String location;
    private String country;

    public Publisher(String name, String location, String country) {
        this.name = name;
        this.location = location;
        this.country = country;
    }
}
