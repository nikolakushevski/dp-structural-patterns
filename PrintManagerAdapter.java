public class PrintManagerAdapter implements PrintManagerInterface {
    PrintManager printManager;

    public PrintManagerAdapter(PrintManager printManager) {
        this.printManager = printManager;
    }

    @Override
    public Card print(User user, Book book) {
        return this.printManager.printCard(user, book);
    }
}
