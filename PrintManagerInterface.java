public interface PrintManagerInterface {
    Card print(User user, Book book);
}
