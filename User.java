import java.time.LocalDateTime;
import java.util.HashMap;

public class User {
    private String name;
    private HashMap<LocalDateTime, Book> history = new HashMap<>();

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addHistory(Book book, LocalDateTime date) {
        this.history.put(date, book);
    }
}
