public interface UserManagerInterface {
    void add(User user);
    void remove(User user);
    void rentBook(User user, Book book);
}
